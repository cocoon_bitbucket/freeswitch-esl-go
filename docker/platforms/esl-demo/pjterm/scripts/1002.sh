#!/bin/sh

# pjsua --null-audio --local-port 5061 --rtp-port 4000  --id=sip:1000@192.168.1.51 --ip-addr=192.168.99.100 --username=1000 --password=1234 --registrar=sip:192.168.1.51 --realm=* --auto-answer=200
# sip:1002@192.168.99.100
exec pjterm \
  --local-port 5062 \
  --rtp-port 20008 \
  --id=sip:1002@192.168.99.100 \
  --ip-addr=192.168.99.100 \
  --username=1002 \
  --password=1234 \
  --realm=* \
  --registrar=sip:192.168.99.100 \
  --log-level 5 --app-log-level 5 \
  --null-audio \
  --no-tcp \
  --auto-answer=200
